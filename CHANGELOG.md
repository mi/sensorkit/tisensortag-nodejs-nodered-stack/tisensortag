# Changelog

## Version 0.0.5
- updates for use with current node.js version (14) and updated libs for this version
- uses now local noble-device, a fork of noble-device, to include the updated versions of noble and hci-bluetooth-socket that are needed for new node.js version
-uses the fork from https://github.com/Yongle-Fu/noble and https://github.com/Yongle-Fu/node-bluetooth-hci-socket
- even more updated version from https://github.com/abandonware might be considered for future use
- works with node.js 14 and Raspberry OS 11
- works with node.js 16 and Raspberry OS 11 (dated November 2022)

## Version 0.0.4
minor bug-fixes

## Version 0.0.3
bug-fixes

## Version 0.0.2
+ threshold and latency for notifys

## Version 0.0.1
made for node-tisensortag based on node-sensortag version 1.2.3
