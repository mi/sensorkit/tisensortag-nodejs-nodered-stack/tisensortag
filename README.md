# tisensortag

A node.js lib for the [TI CC2650 SensorTag (CC2650STK)](http://www.ti.com/tool/cc2650stk).

It is a spinoff of [node-sensortag](https://github.com/sandeepmistry/node-sensortag) by Sandeep Mistry.

It is ...
 * dedicated to CC2650STK SensorTags only (a.k.a. SensorTag 2.0)
 * made for use with new/custom firmware on the SensorTags
 * made to be used by a modified version of node-red-contrib-tisensortag in Node-Red

## Prerequisites
 * Currently only tested with old node.js version (7.10.1). It will (probably) not work out of the box with a new version.
 * [node-gyp install guide](https://github.com/nodejs/node-gyp#installation)
 * [noble prerequisites](https://github.com/sandeepmistry/noble#prerequisites)

## Install
Note: Currently tisensortag is not listed in the npm repository and it needs a manual install.
  * download the files into a local directory, e.g. into ~/tisensortag
  * go into the directory where tisensortag is to be installed
  * perform a npm install from the local source directory
  
```sh
npm install ~/tisensortag/
```

## Usage
To be updated ...

Have a look in [node-sensortag](https://github.com/sandeepmistry/node-sensortag) by Sandeep Mistry.