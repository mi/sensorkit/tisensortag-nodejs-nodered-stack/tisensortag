// http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User's_Guide
var NobleDevice = require('noble-device');
var Common = require('./common');

//advertised data
var ADVERTISED_NAME = "CC2650 SensorTag";
var ADVERTISED_MANUFACTURER_DATA = '0d00030000';
var ADVERTISED_SERVICE_UUID = '0000aa8000001000800000805f9b34fb';

// services and characteristics
var DEVICE_INFORMATION_SERVICE_UUID         = '180a';

var BATTERY_UUID                            = '180f';
var BATTERY_DATA_UUID                       = '2a19';
//var BATTERY_CONFIG_UUID                     = '2902';  //client characteristics configuration
//var BATTERY_REF_UUID                        = '2908';
//var BATTERY_FORM_UUID                       = '2904';  //characteristics presentation format

var SIMPLE_KEY_UUID                         = 'ffe0';
var SIMPLE_KEY_DATA_UUID                    = 'ffe1';

// common for baro-, hygro-, lux- and thermometer
var CONFIG_PERIODEXPONENT_OFFSET = 0;
var CONFIG_PERIODEXPONENT_MASK   = 0x03; // 0000 0011
                                   // raw  00  01  10  11
                                   // val  off 1   2   3 (to be read as 10^val) in units of 10ms in firmware --> 255 * 10^1 * 10ms = 2550 ms
var CONFIG_LATENCY_OFFSET        = 2;
var CONFIG_LATENCY_MASK          = 0xc0; // 0000 1100
                                 // raw  00  01  10  11
                                 // val  1   10  30  60
var CONFIG_THRESHOLD_OFFSET      = 4;
var CONFIG_THRESHOLD_MASK        = 0xf0; // 1111 0000
                                   // raw  0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1110 1111
                                   // +/-  0.00 0.01 0.05 0.10 0.25 0.50 1.00 2.00 0.1% 0.5% 1.0% 2.5% 5.0%  10%  20% forbidden
								   // raw <= 7 --> absolute mode, raw>7 --> relative mode
						   
var BAROMETER_UUID           = 'f000aa4004514000b000000000000000';
var BAROMETER_DATA_UUID      = 'f000aa4104514000b000000000000000';
var BAROMETER_CONFIG_UUID    = 'f000aa4204514000b000000000000000';
var BAROMETER_PERIOD_UUID    = 'f000aa4404514000b000000000000000';

var HYGROMETER_UUID          = 'f000aa2004514000b000000000000000';
var HYGROMETER_DATA_UUID     = 'f000aa2104514000b000000000000000';
var HYGROMETER_CONFIG_UUID   = 'f000aa2204514000b000000000000000';
var HYGROMETER_PERIOD_UUID   = 'f000aa2304514000b000000000000000';

var LUXMETER_UUID            = 'f000aa7004514000b000000000000000';
var LUXMETER_DATA_UUID       = 'f000aa7104514000b000000000000000';
var LUXMETER_CONFIG_UUID     = 'f000aa7204514000b000000000000000';
var LUXMETER_PERIOD_UUID     = 'f000aa7304514000b000000000000000';

var THERMOMETER_UUID         = 'f000aa0004514000b000000000000000';
var THERMOMETER_DATA_UUID    = 'f000aa0104514000b000000000000000';
var THERMOMETER_CONFIG_UUID  = 'f000aa0204514000b000000000000000'; 
var THERMOMETER_PERIOD_UUID  = 'f000aa0304514000b000000000000000'; //scale factor in conig value
							  
											  
var MOVEMENT_UUID            = 'f000aa8004514000b000000000000000';
var MOVEMENT_DATA_UUID       = 'f000aa8104514000b000000000000000';
var MOVEMENT_CONFIG_UUID     = 'f000aa8204514000b000000000000000';
var MOVEMENT_PERIOD_UUID     = 'f000aa8304514000b000000000000000';
var MOVEMENT_GYROMETER_MASK           = 0x0007; // 0000 0000  0000 0111  --> 3 Bit for 3 axes
var MOVEMENT_GYROMETER_OFFSET         = 0;
var MOVEMENT_ACCELEROMETER_MASK       = 0x0238; // 0000 0010  0011 1000  --> 3 Bit for 3 axes + setting for G-range
//var MOVEMENT_ACCELEROMETER_MASK       = 0x0038; // 0000 0000  0011 1000  --> 3 Bit for 3 axes
var MOVEMENT_ACCELEROMETER_OFFSET     = 3;
var MOVEMENT_MAGNETOMETER_MASK        = 0x0040; // 0000 0000  0100 0000  --> 1 Bit for all 3 axes
var MOVEMENT_MAGNETOMETER_OFFSET      = 6;
var MOVEMENT_WOM_MASK                 = 0x0080; // 0000 0000  1000 0000  --> wake on shake flag
var MOVEMENT_WOM_OFFSET               = 7;
var MOVEMENT_GRANGE_MASK              = 0x0300; // 0000 0011  0000 0000  --> 2 Bit G-range flags, default is 10
var MOVEMENT_GRANGE_OFFSET            = 8;
var MOVEMENT_WOM_THRESHOLD_MASK       = 0x3C00; // 0011 1100  0000 0000  --> 4 Bit
var MOVEMENT_WOM_THRESHOLD_OFFSET     = 10;
var MOVEMENT_WOM_TIMEOUT_MASK         = 0xC000; // 1100 0000  0000 0000  --> 2 Bit														 
var MOVEMENT_WOM_TIMEOUT_OFFSET       = 14;

var IO_UUID                  = 'f000aa6404514000b000000000000000';
var IO_DATA_UUID             = 'f000aa6504514000b000000000000000';
var IO_CONFIG_UUID           = 'f000aa6604514000b000000000000000';

/* connection control service */
// TODO

/* OAD update service */
// TODO


var CC2650SensorTag = function(peripheral) {
	NobleDevice.call(this, peripheral);
	Common.call(this);
  
	this.type = 'cc2650';
	//this.firmware = ''; // unused
		
	/*this.batteryConfig = 0;
	this.keysConfig = 0;
	this.barometerConfig = 0;
	this.hygrometerConfig = 0;
	this.luxmeterConfig = 0;
	this.thermometerConfig = 0;*/
	this.movementConfig = 0;
	//this.movementNotifyCount = 0;
  
	this.onBatteryChangeBinded     = this.onBatteryChange.bind(this);
	this.onSimpleKeyChangeBinded   = this.onSimpleKeyChange.bind(this);
	this.onBarometerChangeBinded   = this.onBarometerChange.bind(this);
	this.onHygrometerChangeBinded  = this.onHygrometerChange.bind(this);
	this.onLuxmeterChangeBinded    = this.onLuxmeterChange.bind(this);  
	this.onThermometerChangeBinded = this.onThermometerChange.bind(this);
	this.onMovementChangeBinded    = this.onMovementChange.bind(this);
};

NobleDevice.Util.inherits(CC2650SensorTag, NobleDevice);
/* device information service, a BLE defined profile */
NobleDevice.Util.mixin(CC2650SensorTag, NobleDevice.DeviceInformationService);
NobleDevice.Util.mixin(CC2650SensorTag, Common);
NobleDevice.Util.mixin(CC2650SensorTag, CC2650SensorTag);

CC2650SensorTag.is = function(device) {
  return (device.advertisement.localName === "CC2650 SensorTag");	  
};

CC2650SensorTag.prototype.enableService = function(service, period, latency, threshold, callback) {
	  var service_uuid;
	  var config_uuid;
	  var period_uuid;
		
	  switch (service) {
		case "barometer":
			service_uuid = BAROMETER_UUID;
			config_uuid  = BAROMETER_CONFIG_UUID;
			period_uuid  = BAROMETER_PERIOD_UUID;
			break;
		case "hygrometer":
			service_uuid = HYGROMETER_UUID;
			config_uuid  = HYGROMETER_CONFIG_UUID;
			period_uuid  = HYGROMETER_PERIOD_UUID;
			break;
		case "luxmeter":
			service_uuid = LUXMETER_UUID;
			config_uuid  = LUXMETER_CONFIG_UUID;
			period_uuid  = LUXMETER_PERIOD_UUID;
			break;
		case "thermometer":
			service_uuid = THERMOMETER_UUID;
			config_uuid  = THERMOMETER_CONFIG_UUID;
			period_uuid  = THERMOMETER_PERIOD_UUID;
			break;
		default:
			return false;
	  }
		
	  var config = 0;
	  var periodExponent = 1;
	  
	  if (period < 100) { period = 100; } 
	  if (period > 255000) { period = 255000; }
	  if (period<=2550) {
		  periodExponent = 1;
		  period = Math.floor(period/10);
	  }
	  else if (period<=25500) {
		  periodExponent = 2;
		  period = Math.floor(period/100);
	  }
	  else if (period<=255000) {
		  periodExponent = 3;
	      period = Math.floor(period/1000);
	  }	  
	  
	  config = periodExponent;
	  config |= (latency << CONFIG_LATENCY_OFFSET);
	  config |= (threshold << CONFIG_THRESHOLD_OFFSET);
	  
	  this.writeUInt8Characteristic(service_uuid, period_uuid, period, callback);	  
	  this.writeUInt8Characteristic(service_uuid, config_uuid, config, callback);
	}

// battery level service ist auch bei Noble mit dabei
{ /* battery service, a BLE defined profile */
	// https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.battery_service.xml
	CC2650SensorTag.prototype.readBattery = function(callback) {
	  this.readDataCharacteristic(BATTERY_UUID, BATTERY_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}		
	    this.emit('batteryChange', data.readUInt8(0));
	  }.bind(this));
	};

	/*CC2650SensorTag.prototype.onBatteryChange = function(data) {
	  this.convertBatteryData(data, function(level) {
		this.emit('batteryChange', level);
	  }.bind(this));
	};*/
	
	CC2650SensorTag.prototype.onBatteryChange = function(data) {	  
	  this.emit('batteryChange', data.readUInt8(0));
	};	

	CC2650SensorTag.prototype.notifyBattery = function(callback) {
	  this.notifyCharacteristic(BATTERY_UUID, BATTERY_DATA_UUID, true, this.onBatteryChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifyBattery = function(callback) {
	  this.notifyCharacteristic(BATTERY_UUID, BATTERY_DATA_UUID, false, this.onBatteryChangeBinded, callback);
	};	
}

{ /* simple key service, a BLE defined profile */
	CC2650SensorTag.prototype.onSimpleKeyChange = function(data) {
	  this.convertSimpleKeyData(data, function(/*left, right, ...*/) {
		var emitArguments = Array.prototype.slice.call(arguments);
		emitArguments.unshift('simpleKeyChange');

		this.emit.apply(this, emitArguments);
	  }.bind(this));
	};

	CC2650SensorTag.prototype.convertSimpleKeyData = function(data, callback) {
	  var b = data.readUInt8(0);

	  var left = (b & 0x2) ? true : false;
	  var right = (b & 0x1) ? true : false;
	  var reedRelay = (b & 0x4) ? true : false;

	  callback(left, right, reedRelay);
	};
	
	CC2650SensorTag.prototype.configSimpleKey = function(key, value, callback) {
		switch (key) {
		
		}
	}

	CC2650SensorTag.prototype.notifySimpleKey = function(callback) {
	  this.notifyCharacteristic(SIMPLE_KEY_UUID, SIMPLE_KEY_DATA_UUID, true, this.onSimpleKeyChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifySimpleKey = function(callback) {
	  this.notifyCharacteristic(SIMPLE_KEY_UUID, SIMPLE_KEY_DATA_UUID, false, this.onSimpleKeyChangeBinded, callback);
	};
}

{ /* luxmeter / light service (OPT), a TI custom defined profile */	
	CC2650SensorTag.prototype.setLuxmeterPeriodExtended = function(period, callback) {
	  this.writePeriodCharacteristicExtended(LUXMETER_UUID, LUXMETER_PERIOD_UUID, LUXMETER_CONFIG_UUID, period, callback);	  
	};
	
	CC2650SensorTag.prototype.enableLuxmeter = function(callback) {
	  this.enableConfigCharacteristic(LUXMETER_UUID, LUXMETER_CONFIG_UUID, callback);
	};
	
	CC2650SensorTag.prototype.disableLuxmeter = function(callback) {
	  this.disableConfigCharacteristic(LUXMETER_UUID, LUXMETER_CONFIG_UUID, callback);
	};

	CC2650SensorTag.prototype.notifyLuxmeter = function(callback) {
	  this.notifyCharacteristic(LUXMETER_UUID, LUXMETER_DATA_UUID, true, this.onLuxmeterChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifyLuxmeter = function(callback) {
	  this.notifyCharacteristic(LUXMETER_UUID, LUXMETER_DATA_UUID, false, this.onLuxmeterChangeBinded, callback);
	};
	
	CC2650SensorTag.prototype.readLuxmeter = function(callback) {
	  this.readDataCharacteristic(LUXMETER_UUID, LUXMETER_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertLuxmeterData(data, function(lux) {
		  callback(null, lux);
		}.bind(this));
	  }.bind(this));
	 };

	CC2650SensorTag.prototype.onLuxmeterChange = function(data) {
	  this.convertLuxmeterData(data, function(lux) {
		this.emit('luxmeterChange', lux);
	  }.bind(this));
	};

	CC2650SensorTag.prototype.convertLuxmeterData = function(data, callback) {
	  var rawLux = data.readUInt16LE(0);

	  var exponent = (rawLux & 0xF000) >> 12;
	  var mantissa = (rawLux & 0x0FFF);

	  var flLux = mantissa * Math.pow(2, exponent) / 100.0;

	  callback(flLux);
	};	
}

{ /* thermometer / temperature  service (TMP), a TI custom defined profile */
	CC2650SensorTag.prototype.convertThermometerData = function(data, callback) {
	  var ambientTemperature = data.readInt16LE(2) / 128.0;
	  var objectTemperature = data.readInt16LE(0) / 128.0;

	  callback(objectTemperature, ambientTemperature);
	};

	CC2650SensorTag.prototype.disableThermometer = function(callback) {
	  this.disableConfigCharacteristic(THERMOMETER_UUID, THERMOMETER_CONFIG_UUID, callback);
	};

	CC2650SensorTag.prototype.readThermometer = function(callback) {
	  this.readDataCharacteristic(THERMOMETER_UUID, THERMOMETER_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertThermometerData(data, function(objectTemperature, ambientTemperature) {
		  callback(null, objectTemperature, ambientTemperature);
		}.bind(this));
	  }.bind(this));
	};

	CC2650SensorTag.prototype.onThermometerChange = function(data) {
	  this.convertThermometerData(data, function(objectTemperature, ambientTemperature) {
		this.emit('thermometerChange', objectTemperature, ambientTemperature);
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyThermometer = function(callback) {
	  this.notifyCharacteristic(THERMOMETER_UUID, THERMOMETER_DATA_UUID, true, this.onThermometerChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifyThermometer = function(callback) {
	  this.notifyCharacteristic(THERMOMETER_UUID, THERMOMETER_DATA_UUID, false, this.onThermometerChangeBinded, callback);
	};
}

{ /* hygrometer / humidity service (HUM), a TI custom defined profile */
	CC2650SensorTag.prototype.convertHygrometerData = function(data, callback) {
	  var temperature = -40 + ((165  * data.readUInt16LE(0)) / 65536.0);
	  var humidity = data.readUInt16LE(2) * 100 / 65536.0;
	  //TODO: Math.round
	  callback(temperature, humidity);
	};	

	CC2650SensorTag.prototype.disableHygrometer = function(callback) {
	  this.disableConfigCharacteristic(HYGROMETER_UUID, HYGROMETER_CONFIG_UUID, callback);
	};

	CC2650SensorTag.prototype.readHygrometer = function(callback) {
	  this.readDataCharacteristic(HYGROMETER_UUID, HYGROMETER_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertHygrometerData(data, function(temperature, humidity) {
		  callback(null, temperature, humidity);
		});
	  }.bind(this));
	};

	CC2650SensorTag.prototype.onHygrometerChange = function(data) {
	  this.convertHygrometerData(data, function(temperature, humidity) {
		this.emit('hygrometerChange', temperature, humidity);
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyHygrometer = function(callback) {
	  this.notifyCharacteristic(HYGROMETER_UUID, HYGROMETER_DATA_UUID, true, this.onHygrometerChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifyHygrometer = function(callback) {
	  this.notifyCharacteristic(HYGROMETER_UUID, HYGROMETER_DATA_UUID, false, this.onHygrometerChangeBinded, callback);
	};
}
	
{ /* barometer / barometric pressure service (BAR), a TI custom defined profile */
	CC2650SensorTag.prototype.convertBarometerData = function(data, callback) {
	  // data is returned as
	  // Firmware 0.89 16 bit single precision float
	  // Firmware 1.01 24 bit single precision float
	  
	  //alkur added: flTempBMP is returned as well

	  var flTempBMP;
	  var flPressure;

	  if (data.length > 4) {
		// Firmware 1.01

		flTempBMP = (data.readUInt32LE(0) & 0x00ffffff)/ 100.0;
		flPressure = ((data.readUInt32LE(2) >> 8) & 0x00ffffff) / 100.0;
	  } else {
		// Firmware 0.89

		var tempBMP = data.readUInt16LE(0);
		var tempExponent = (tempBMP & 0xF000) >> 12;
		var tempMantissa = (tempBMP & 0x0FFF);
		flTempBMP = tempMantissa * Math.pow(2, tempExponent) / 100.0;

		var tempPressure = data.readUInt16LE(2);
		var pressureExponent = (tempPressure & 0xF000) >> 12;
		var pressureMantissa = (tempPressure & 0x0FFF);
		flPressure = pressureMantissa * Math.pow(2, pressureExponent) / 100.0;
	  }

	  callback(flPressure, flTempBMP);
	};

	CC2650SensorTag.prototype.disableBarometer = function(callback) {
	  this.disableConfigCharacteristic(BAROMETER_UUID, BAROMETER_CONFIG_UUID, callback);
	};

	CC2650SensorTag.prototype.readBarometer = function(callback) {
	  this.readDataCharacteristic(BAROMETER_UUID, BAROMETER_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertBarometerData(data, function(pressure) {
		  callback(null, pressure, temperature);
		}.bind(this));
	  }.bind(this));
	};

	CC2650SensorTag.prototype.onBarometerChange = function(data) {
	  this.convertBarometerData(data, function(pressure, temperature) {
		this.emit('barometerChange', pressure, temperature);
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyBarometer = function(callback) {
	  this.notifyCharacteristic(BAROMETER_UUID, BAROMETER_DATA_UUID, true, this.onBarometerChangeBinded, callback);
	};

	CC2650SensorTag.prototype.unnotifyBarometer = function(callback) {
	  this.notifyCharacteristic(BAROMETER_UUID, BAROMETER_DATA_UUID, false, this.onBarometerChangeBinded, callback);
	};
}

{ /* movement / mpu9250 service (MOV), a TI custom defined profile */
	CC2650SensorTag.prototype.setMovementPeriod = function(period, callback) {
	  this.writePeriodCharacteristic(MOVEMENT_UUID, MOVEMENT_PERIOD_UUID, period, callback);
	};
	
	CC2650SensorTag.prototype.enableMovement = function(callback) {
	  this.writeUInt16LECharacteristic(MOVEMENT_UUID, MOVEMENT_CONFIG_UUID, this.movementConfig, callback);
	};

	/*CC2650SensorTag.prototype.enableMovement = function(config, callback) {
	  this.movementConfig = config;	  
	  this.writeUInt16LECharacteristic(MOVEMENT_UUID, MOVEMENT_CONFIG_UUID, this.movementConfig, callback);
	};
	
	CC2650SensorTag.prototype.enableMovement = function(period, config, callback) {
	  this.movementConfig = config;
	  this.writePeriodCharacteristic(MOVEMENT_UUID, MOVEMENT_PERIOD_UUID, period, callback);
	  this.writeUInt16LECharacteristic(MOVEMENT_UUID, MOVEMENT_CONFIG_UUID, this.movementConfig, callback);
	};*/

	CC2650SensorTag.prototype.disableMovement = function(mask, callback) {
	  //this.movementConfig &= ~mask;
	  this.movementConfig = 0;

	  if (this.movementConfig === 0) {
		this.writeUInt16LECharacteristic(MOVEMENT_UUID, MOVEMENT_CONFIG_UUID, 0x0000, callback);
	  } else if (typeof(callback) === 'function') {
		callback();
	  }
	};

	CC2650SensorTag.prototype.notifyMovement = function(callback) {
	  //this.movementNotifyCount++;

	  //if (this.movementNotifyCount === 1) {
		this.notifyCharacteristic(MOVEMENT_UUID, MOVEMENT_DATA_UUID, true, this.onMovementChangeBinded, callback);
	  //} else if (typeof(callback) === 'function') {
	  //callback();
	  //}
	};

	CC2650SensorTag.prototype.unnotifyMovement = function(callback) {
	  //this.movementNotifyCount--;

	  //if (this.movementNotifyCount === 0) {
		this.notifyCharacteristic(MOVEMENT_UUID, MOVEMENT_DATA_UUID, false, this.onMovementChangeBinded, callback);
	  //} else if (typeof(callback) === 'function') {
		//callback();
	  //}
	};
	
	CC2650SensorTag.prototype.setMovementWomEnable = function(callback) {
	  this.movementConfig |= MOVEMENT_WOM_MASK;
	};

	CC2650SensorTag.prototype.setMovementWomDisable = function(callback) {
	  this.disableMovement(MOVEMENT_WOM_MASK, callback);
	};
	
	CC2650SensorTag.prototype.setMovementWomThreshold = function(threshold, callback) {	  	  
	  //this.movementConfig |= ((threshold << MOVEMENT_WOM_THRESHOLD_OFFSET) & MOVEMENT_WOM_THRESHOLD_MASK);	  
	  this.movementConfig |= (threshold << MOVEMENT_WOM_THRESHOLD_OFFSET);
	};
	
	CC2650SensorTag.prototype.setMovementWomTimeout = function(timeout, callback) {	      
	  //this.movementConfig |= ((timeout << MOVEMENT_WOM_TIMEOUT_OFFSET) & MOVEMENT_WOM_TIMEOUT_MASK);	  
	  this.movementConfig |= (timeout << MOVEMENT_WOM_TIMEOUT_OFFSET);
	};
	
	CC2650SensorTag.prototype.setMovementGRange = function(gRange, callback) {	      
	  //this.movementConfig |= ((gRange << MOVEMENT_GRANGE_OFFSET) & MOVEMENT_GRANGE_MASK);  
	  this.movementConfig |= (0x02 << MOVEMENT_GRANGE_OFFSET);
	};
	
	CC2650SensorTag.prototype.setMovementConfig = function(gRange, womEnable, womThreshold, womTimeout, callback) {	      
	  this.movementConfig |= MOVEMENT_WOM_MASK;
	  this.movementConfig |= ((gRange << MOVEMENT_GRANGE_OFFSET) & MOVEMENT_GRANGE_MASK);
	  this.movementConfig |= ((womTimeout << MOVEMENT_WOM_TIMEOUT_OFFSET) & MOVEMENT_WOM_TIMEOUT_MASK);	  
	  this.movementConfig |= ((womThreshold << MOVEMENT_WOM_THRESHOLD_OFFSET) & MOVEMENT_WOM_THRESHOLD_MASK);
	};
	
	CC2650SensorTag.prototype.onMovementChange = function(data) {
	  this.convertMovementData(data, function(xA, yA, zA, xG, yG, zG, xM, yM, zM) {
		if (this.movementConfig & MOVEMENT_ACCELEROMETER_MASK) {
		  this.emit('accelerometerChange', xA, yA, zA);
		}

		if (this.movementConfig & MOVEMENT_GYROMETER_MASK) {
		  this.emit('gyrometerChange', xG, yG, zG);
		}

		if (this.movementConfig & MOVEMENT_MAGNETOMETER_MASK) {
		  this.emit('magnetometerChange', xM, yM, zM);
		}
	  }.bind(this));
	};

	CC2650SensorTag.prototype.convertMovementData = function(data, callback) {
	  // 250 deg/s range, with a resolution of ~0.0078125
	  var xG = data.readInt16LE(0) / 128.0;
	  var yG = data.readInt16LE(2) / 128.0;
	  var zG = data.readInt16LE(4) / 128.0;

	  // TODO: other G-ranges than 8G are possible!
	  // we specify 8G range in setup
	  var xA = data.readInt16LE(6) / 4096.0;
	  var yA = data.readInt16LE(8) / 4096.0;
	  var zA = data.readInt16LE(10)/ 4096.0;

	  // magnetometer (page 50 of http://www.invensense.com/mems/gyro/documents/RM-MPU-9250A-00.pdf)
	  var xM = data.readInt16LE(12) * 4912.0 / 32768.0;
	  var yM = data.readInt16LE(14) * 4912.0 / 32768.0;
	  var zM = data.readInt16LE(16) * 4912.0 / 32768.0;

	  callback(xA, yA, zA, xG, yG, zG, xM, yM, zM);
	};
	
	CC2650SensorTag.prototype.enableAccelerometer = function(callback) {
	  this.movementConfig |= MOVEMENT_ACCELEROMETER_MASK;
	};

	CC2650SensorTag.prototype.disableAccelerometer = function(callback) {
	  this.disableMovement(MOVEMENT_ACCELEROMETER_MASK, callback);
	};

	CC2650SensorTag.prototype.readAccelerometer  = function(callback) {
	  this.readDataCharacteristic(MOVEMENT_UUID, MOVEMENT_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertMovementData(data, function(x, y, z) {
		  callback(null, x, y, z);
		}.bind(this));
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyAccelerometer = function(callback) {
	  this.notifyMovement(callback);
	};

	CC2650SensorTag.prototype.unnotifyAccelerometer = function(callback) {
	  this.unnotifyMovement(callback);
	};
	
	CC2650SensorTag.prototype.enableGyrometer = function(callback) {
	  this.movementConfig |= MOVEMENT_GYROMETER_MASK;
	};

	CC2650SensorTag.prototype.disableGyrometer = function(callback) {
	  this.disableMovement(MOVEMENT_GYROMETER_MASK, callback);
	};

	CC2650SensorTag.prototype.readGyrometer = function(callback) {
	  this.readDataCharacteristic(MOVEMENT_UUID, MOVEMENT_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertMovementData(data, function(x, y, z, xG, yG, zG) {
		  callback(null, xG, yG, zG);
		}.bind(this));
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyGyrometer = function(callback) {
	  this.notifyMovement(callback);
	};

	CC2650SensorTag.prototype.unnotifyGyrometer = function(callback) {
	  this.unnotifyMovement(callback);
	};

	CC2650SensorTag.prototype.enableMagnetometer = function(callback) {
	  this.movementConfig |= MOVEMENT_MAGNETOMETER_MASK;	  
	};

	CC2650SensorTag.prototype.disableMagnetometer = function(callback) {
	  this.disableMovement(MOVEMENT_MAGNETOMETER_MASK, callback);
	};

	CC2650SensorTag.prototype.readMagnetometer = function(callback) {
	  this.readDataCharacteristic(MOVEMENT_UUID, MOVEMENT_DATA_UUID, function(error, data) {
		if (error) {
		  return callback(error);
		}

		this.convertMovementData(data, function(xA, yA, zA, xG, yG, zG, xM, yM, zM) {
		  callback(null, xM, yM, zM);
		}.bind(this));
	  }.bind(this));
	};

	CC2650SensorTag.prototype.notifyMagnetometer = function(callback) {
	  this.notifyMovement(callback);
	};

	CC2650SensorTag.prototype.unnotifyMagnetometer = function(callback) {
	  this.unnotifyMovement(callback);
	};
}

{ /* io service, a TI custom defined profile */
	CC2650SensorTag.prototype.readIoData = function(callback) {
	  this.readUInt8Characteristic(IO_UUID, IO_DATA_UUID, callback);
	};

	CC2650SensorTag.prototype.writeIoData = function(value, callback) {
	  this.writeUInt8Characteristic(IO_UUID, IO_DATA_UUID, value, callback);
	};

	CC2650SensorTag.prototype.readIoConfig = function(callback) {
	  this.readUInt8Characteristic(IO_UUID, IO_CONFIG_UUID, callback);
	};

	CC2650SensorTag.prototype.writeIoConfig = function(value, callback) {
	  this.writeUInt8Characteristic(IO_UUID, IO_CONFIG_UUID, value, callback);
	};
}

{ /* oad service, a TI custom defined profile */
	//TODO
}

module.exports = CC2650SensorTag;
