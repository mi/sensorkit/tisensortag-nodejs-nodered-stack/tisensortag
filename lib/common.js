// http://processors.wiki.ti.com/index.php/SensorTag_User_Guide

function SensorTagCommon() {

}

SensorTagCommon.prototype.toString = function() {
  return JSON.stringify({
    id: this.id,
    type: this.type
  });
};

SensorTagCommon.prototype.writePeriodCharacteristic = function(serviceUuid, characteristicUuid, period, callback) {
  period /= 10; // input is scaled by units of 10ms

  if (period < 1) {
    period = 1;
  } else if (period > 255) {
    period = 255;
  }

  this.writeUInt8Characteristic(serviceUuid, characteristicUuid, period, callback);
};

SensorTagCommon.prototype.writePeriodCharacteristicExtended = function(serviceUuid, periodUuid, configUuid, period, callback) {
  // extendedConfig sollte einfach zu berechnender Faktor sein
  // 1 = 10^1 ms =    10ms Auflösung, Wertebereich von 100 bis 2550 ms
  // 2 = 10^2 ms =   100ms Auflösung, Wertebereich von 1000 bis 25500 ms
  // 3 = 10^3 ms =  1000ms Auflösung, Wertebereich von 10000 bis 255000 ms bzw. 10 bis 255s
  
  // no longer supported:
  // 4 = 10^4 ms =    10s  Auflösung, Wertebereich von 100000 bis 2550000 ms bzw. 100 bis 2550s
  // 5 = 10^5 ms =   100s  Auflösung, Wertebereich von 1000000 bis 25500000 ms bzw. 1000 bis 25500s --> max. ist 4294s
  
  // es wird also eigentlich ein Logarithmus zur Basis 10 berechnet
  // 10ms ist auch gerade die Granularität in der Firmware
  
  // max. Zeit für Timer (zum Schlafen) wird als 32Bit Zahl für µs (geteilt durch Ticks) verarbeitet
  // 2^32 µs = 4294s --> etwas über eine Stunde ist plausible Annahme bei Annahme von 1 Tick/µs
  
  var config=1;
  
  if (period < 100) {
    period = 100;
  } else if (period > 4294000) {
    period = 4294000;
  }
  
  if (period<=2550) {
	config = 1;
	period = Math.floor(period/10);
  }
  else if (period<=25500) {
	config = 2;
	period = Math.floor(period/100);
  }
  else if (period<=255000) {
	config = 3;
	period = Math.floor(period/1000);
  }
  else {
	return false;
  }
  /*if (period<=2550000) {
	config = 4;
	period = Math.floor(period/10000);
  }
  else if (period<=4294000) {
	config = 5;
	period = Math.floor(period/100000);
  }*/

  this.writeUInt8Characteristic(serviceUuid, configUuid, config, callback);  
  this.writeUInt8Characteristic(serviceUuid, periodUuid, period, callback);
};

SensorTagCommon.prototype.enableConfigCharacteristicExtended = function(serviceUuid, characteristicUuid, config, callback) {
  this.writeUInt8Characteristic(serviceUuid, characteristicUuid, config, callback);
};

SensorTagCommon.prototype.enableConfigCharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.writeUInt8Characteristic(serviceUuid, characteristicUuid, 0x01, callback);
};

SensorTagCommon.prototype.disableConfigCharacteristic = function(serviceUuid, characteristicUuid, callback) {
  this.writeUInt8Characteristic(serviceUuid, characteristicUuid, 0x00, callback);
};

module.exports = SensorTagCommon;
